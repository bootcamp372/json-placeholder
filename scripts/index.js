window.onload = function () {
    const getTodoBtn = document.getElementById("getTodoBtn");
    const getTodosByUserIDBtn = document.getElementById("getTodosByUserIDBtn");
    const getAllTodosBtn = document.getElementById("getAllTodosBtn");
    const clearTodosBtn = document.getElementById("clearTodosBtn");

    const getUserBtn = document.getElementById("getUserBtn");
    const getAllUsers = document.getElementById("getAllUsersBtn");
    const clearUsersBtn = document.getElementById("clearUsersBtn");
    const closeAddressModalBtn = document.getElementById("closeAddressModalBtn");


    getUsers();
    if (getTodoBtn) {
        getTodoBtn.onclick = getToDoByID;
    }

    if (getAllTodosBtn) {
        getAllTodosBtn.onclick = getToDos;
    }

    if (clearTodosBtn) {
        clearTodosBtn.onclick = clearToDos;
    }

    if (getTodosByUserIDBtn) {
        getTodosByUserIDBtn.onclick = getTodosByUserID;
    }

    if (getUserBtn) {
        getUserBtn.onclick = getUser;
    }

    if (clearUsersBtn) {
        clearUsersBtn.onclick = clearUserTable;
    }

    if (getAllUsers) {
        getAllUsers.onclick = getUsers;
    }

    if (closeAddressModalBtn) {
        closeAddressModalBtn.onclick = clearAddressTable;
    }

};

function getToDoByID() {

    const todoText = document.getElementById("todoText");
    const inputValue = document.getElementById("todoIDInput").value;
    url = "https://jsonplaceholder.typicode.com/todos/" + inputValue;
    console.log(url);
    clearToDos();
    fetch(url)
        .then(response => response.json())
        .then(jsonData => {
            todoText.innerHTML = JSON.stringify(jsonData);
            console.dir(jsonData);
            //  todoText.innerHTML = "userID: " + jsonData.userId + " | " + "id: " + jsonData.id + " | " + "title: " + jsonData.title + " | " + "completed: " + jsonData.completed;
        })
        .catch(err => console.error(err));
}

function getTodosByUserID() {
    const todoText = document.getElementById("todoText");
    const todosUserIDInput = document.getElementById("todosUserIDInput").value;
    url = "https://jsonplaceholder.typicode.com/todos/";
    console.log(url);
    clearToDos();
    fetch(url)
        .then(response => response.json())
        .then(jsonData => {
            let filteredByUserID = jsonData.filter(function (todo) {
                return todo.userId == todosUserIDInput;
            });
            filteredByUserID.forEach(todo => {
                let div = document.createElement("div");
                div.id = "user-id-" + todo.userId;
                div.innerHTML = JSON.stringify(todo);
                todoText.appendChild(div);
            });
            // for (let i = 0; i < jsonData.length; i++) {
            //     if (jsonData[i].userId == todosUserIDInput) {
            //         let div = document.createElement("div");
            //         div.id = "user-id-" + jsonData[i].userId;
            //         div.innerHTML = JSON.stringify(jsonData[i]);
            //         todoText.appendChild(div);   
            //     }             
            // } 
        })
        .catch(err => console.error(err));
}

function getToDos() {

    const todoText = document.getElementById("todoText");
    url = "https://jsonplaceholder.typicode.com/todos/";
    console.log(url);
    clearToDos();
    fetch(url)
        .then(response => response.json())
        .then(jsonData => {
            for (let i = 0; i < jsonData.length; i++) {
                let div = document.createElement("div");
                div.id = "id-" + jsonData[i].id;
                div.innerHTML = JSON.stringify(jsonData[i]);
                todoText.appendChild(div);
            }
        })
        .catch(err => console.error(err));
}

function getUser() {

    const userID = document.getElementById("userIDInput").value;
    url = "https://jsonplaceholder.typicode.com/users/" + userID;

    console.log(url);
    clearUserTable();
    fetch(url)
        .then(response => response.json())
        .then(jsonData => {
            loadTable(jsonData);
        })
        .catch(err => console.error(err));
}

function getUsers() {

    url = "https://jsonplaceholder.typicode.com/users";
    console.log(url);
    clearUserTable();
    fetch(url)
        .then(response => response.json())
        .then(jsonData => {

            for (let i = 0; i < jsonData.length; i++) {
                loadTable(jsonData[i]);
            }
        })
        .catch(err => console.error(err));
}

// var yahooOnly = JSON.parse(jsondata).filter(function (entry) {
//     return entry.website === 'yahoo';
// });

function clearUserTable() {
    const usersTblBody = document.getElementById("usersTblBody");
    if (usersTblBody) {
        usersTblBody.innerHTML = "";
    }
}

function clearToDos() {
    const todoText = document.getElementById("todoText").innerHTML = "";
    if (todoText) {
        todoText.innerHTML = "";
    }
}

function clearAddressTable(){
    const addressTblBody = document.getElementById("addressTblBody");
    if (addressTblBody) {
        addressTblBody.innerHTML = "";
    }
}

function loadTable(user) {
    const userTbody = document.getElementById("usersTblBody");
    if (userTbody) {
        let row = userTbody.insertRow(-1);
        let cell1 = row.insertCell(0);
        let cell2 = row.insertCell(1);
        let cell3 = row.insertCell(2);
        let cell4 = row.insertCell(3);
        let cell5 = row.insertCell(4);
        let cell6 = row.insertCell(5);
        let cell7 = row.insertCell(6);

        cell1.innerHTML = user.id;
        cell2.innerHTML = user.name;
        cell3.innerHTML = user.username;
        cell4.innerHTML = user.email;
        cell5.innerHTML = user.phone;
        cell6.innerHTML = user.website;
        cell7.innerHTML = '<i class="fa fa-envelope"></i>';
        cell7.className = 'td-hover text-center';
        cell7.onclick = function () {

            const addressTbody = document.getElementById("addressTblBody");
            const addressModal = new bootstrap.Modal(document.getElementById("addressModal"), {});

            if (addressTbody) {
                let addressRow = addressTbody.insertRow(-1);
                let addressCell1 = addressRow.insertCell(0);
                let addressCell2 = addressRow.insertCell(1);
                let addressCell3 = addressRow.insertCell(2);
                let addressCell4 = addressRow.insertCell(3);

                addressCell1.innerHTML = user.address.street;
                addressCell2.innerHTML = user.address.suite;
                addressCell3.innerHTML = user.address.city;
                addressCell4.innerHTML = user.address.zipcode;
                addressModal.show();
            }
        }
    }

}